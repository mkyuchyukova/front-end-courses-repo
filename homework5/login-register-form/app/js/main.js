/**
 * Master application
 */


 /*
  * TODO: Router on hashchange, bind/unbind events: login,register; **extra: models
  */
// window.Env = window.Env || {};

(function (GLOBAL) {
	window.addEventListener("hashchange", hashChange);
	window.onload = function(){ hashChange(); };
})(window);

function hashChange(){
	if(location.hash == ''){
			location.hash = '#login';
		} 

		var currentHash = location.hash.slice(1);

		var currentSection = document.getElementById("placeholder-" + currentHash);
		var currentLink = document.getElementById(currentHash);

		var previousSection = document.querySelectorAll(".hidden-section.section-active")[0];
		var previousLink = document.querySelectorAll(".nav-link.nav-link-active")[0];

		currentLink.classList.add('nav-link-active');
		currentSection.classList.add('section-active');

		if(currentLink != previousLink){
			previousLink.classList.remove('nav-link-active');
			previousSection.classList.remove('section-active');
		}
		
};

function registerUser(){
   
    var username = document.getElementById("reg-username").value,
		password = document.getElementById("reg-password").value;
 		
	API.user.add(username, password);
	location.hash = '#login';
}

function login(){
	var username = document.getElementById("login-username").value,
		password = document.getElementById("login-password").value;

	API.user.check(username, password, function myCallbackFunction(data){
	    if(data.length != 0){
	    	location.hash = "#home";
	    }
	    else{
	    	alert("Bad input!");
	    }
	} );
}

function loadHTML(templateName, callback) {
    if (window.XMLHttpRequest) {
        ajaxRequest = new XMLHttpRequest();
    }

    ajaxRequest.onreadystatechange = function () {
        if (ajaxRequest.readyState === 4 ) {
            if (ajaxRequest.status === 200 && ajaxRequest.status < 300) {
                //return the html of the template as plain string
                callback && callback(ajaxRequest.responseText);
            }
        }
    };
    
    console.log('LOADER: '+ typeof templateName);
    //get the needed template
    ajaxRequest.open("GET", "app/templates/" + templateName + ".html", true);
    ajaxRequest.send();

}