/*  
	Router handler
*/
	// Cache the result:
	var cache = {};
 	// A hash to store our routes:
    var routes = {};

    var loggedUser,
        userID;

    // initialize the html element where the content would be loaded
    var el = null;

	function loadHTML(templateName, callback) {
	    if (window.XMLHttpRequest) {
	        ajaxRequest = new XMLHttpRequest();
	    }

	    ajaxRequest.onreadystatechange = function () {
	        if (ajaxRequest.readyState === 4 ) {
	            if (ajaxRequest.status === 200 && ajaxRequest.status < 300) {
	                //return the html of the template as plain string
	                callback && callback(ajaxRequest.responseText);

                    if(templateName == "home"){
                        if(userID != undefined){
                            var responseData = ajaxRequest.responseText;

                            if(responseData.indexOf("documents-list") > -1){ 
                                document.getElementById("info-name").innerHTML = loggedUser; 
                                var docs = API.documents.getAll(userID.toString(), function myCallbackFunction(data){
                                    document.getElementById("documents-list").innerHTML = "";
                                    for(var i=0; i<data.length; i++){
                                        document.getElementById("documents-list").innerHTML += "<button id=\"btn-" + data[i].id + "\" onclick=\"loadListItem("+ data[i].id +")\" class=\"btn-doc-item\"><div class=\"doc-item\"><h2 id=\"doc-item-title\">" + data[i].title + "</h2><br/><span>" + data[i].date + "</span><p id=\"doc-item-text\">" + data[i].text + "</p></div></button>";          
                                    }
                                });
                            }
                        } else{
                            location.hash = "/login";
                        }                        
                    }                                            
	            }
	        }
	    };
	    
	    console.log('LOADER: '+ typeof templateName);
	    //get the needed template
	    ajaxRequest.open("GET", "templates/" + templateName + ".html", true);
	    ajaxRequest.send();
	}

    // loads the template 
    function route (path, templateId, controller) {
      routes[path] = {templateId: templateId, controller: controller};
    }

    // loads the template
    function router () {
      // Lazy load view element:
      	el = el || document.getElementById('view');
        // Current route url (getting rid of '#' in hash as well):
        var url = location.hash.slice(1) || '/';
        // Get route by url:
        var route = routes[url];

        // Do we have both a view and a route?
        if (el && route.controller) {
          // Render route template with John Resig's template engine:
            loadHTML(route.templateId, function(data){el.innerHTML=data;});
        }
    }

    // Listen on hash change:
    window.addEventListener('hashchange', router);
    // Listen on page load:
    window.addEventListener('load', router);

    