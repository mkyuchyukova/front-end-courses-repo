/*
	Calling the route funciton for each hash tag
*/

	route('/', 'login', function () {});

	route('/login', 'login', function () {});

	route('/register', 'register', function () {});

	route('/home', 'home', function () {});

function registerUser(){
   
    var username = document.getElementById("reg-username").value,
		password = document.getElementById("reg-password").value;
 	console.info(username + ", " + password);	
	
	if(validate("register") == true){
		API.user.add(username, password);
		location.hash = '/login';
	}	
}

function login(){
	var username = document.getElementById("login-username").value,
		password = document.getElementById("login-password").value;

	
	if(validate("login") == true){
		API.user.check(username, password, function myCallbackFunction(data){
		    if(data.length != 0){
		    	location.hash = "/home";
		    	userID = data[0].id;
		    	loggedUser = data[0].username;
		    }
		    else{
		    	// alert("Bad input!");
		    	document.getElementById("fieldset-error").classList.remove("fieldset-login-error");
		    	
		    	document.getElementById("login-username").value = "";
				document.getElementById("login-password").value = "";

		    }
		} );
	}
}

function addDocument(){
	var docId = document.getElementById("doc-id").value,
		title = document.getElementById("doc-title").value,
		text = document.getElementById("doc-text").value;

	if(userID != undefined){
		if(docId == ""){
			console.info("Add new document.");
			API.documents.add({title: title, text: text, userid: userID.toString(), url: null, note: null}, function myCallbackFunction(data){
				if(data.length != 0){
			    	document.getElementById("documents-list").innerHTML = "";
			    	getAllDocuments();
			    	clearInputs();			
			    }
			    else{
			    	console.info("Error creating!");
			    }
			});
		} else{
			console.info("Update document id: " + docId);

			API.documents.edit(docId.toString(), {title: title, text: text}, function myCallbackFunction(data){
				if(data.length != 0){
					console.info("Updated document id: " + docId + " successful.");

			    	document.getElementById("documents-list").innerHTML = "";
			    	getAllDocuments();
			    	console.info("getAllDocuments(): ");
			    	clearInputs();	
			    	console.info("clearInputs(): ");		
			    }
			    else{
			    	console.info("Error updating!");
			    }
			});
		}
	} else{
		location.hash = "/login";
	}
}

function getAllDocuments(){
	if(userID != undefined){
		var docs = API.documents.getAll(userID.toString(), function myCallbackFunction(data){
			
			document.getElementById("documents-list").innerHTML = "";
			for(var i=0; i<data.length; i++){
				document.getElementById("documents-list").innerHTML += "<button id=\"btn-" + data[i].id + "\" onclick=\"loadListItem("+ data[i].id +")\" class=\"btn-doc-item\"><div class=\"doc-item\"><h2 id=\"doc-item-title\">" + data[i].title + "</h2><br/><span id=\"doc-item-date\">" + data[i].date + "</span><p id=\"doc-item-text\">" + data[i].text + "</p></div></button>";          
			}
		});
	}
	else{
		location.hash = "/login";
	}
}

function clearInputs(){
	document.getElementById("doc-id").value = "";
	document.getElementById("doc-title").value = "";
	document.getElementById("doc-text").value = "";
}

function loadListItem(docID){
	API.documents.getDoc(docID.toString(), function myCallbackFunction(data){
		document.getElementById("doc-id").value = data[0].id;
		document.getElementById("doc-title").value = data[0].title;
		document.getElementById("doc-text").value = data[0].text;
	});

	var currentBtn = document.getElementById("btn-" + docID.toString()),
		activeBtn = document.getElementsByClassName("btn-active")[0];

		console.info("Current: " + document.getElementById("btn-" + docID.toString()));
		currentBtn.classList.add("btn-active");

		if(activeBtn != undefined && activeBtn != currentBtn){
			console.info("Active found: " + activeBtn);
			activeBtn.classList.remove("btn-active");
		} else{
			console.info("Active not found. ");
		}
}

function removeDocument(){
	console.info("Remove Document: ");
	if(document.getElementById("doc-id").value != ""){

		 var r = confirm("Do you want to proceed deleting document " + document.getElementById("doc-title").value + "?");
		    if (r == true) {
		        API.documents.removeDoc(document.getElementById("doc-id").value.toString());
				getAllDocuments();
		    } 		
	}	
}

function validate(type){
	var errors = [];

	var ck_name = /^[A-Za-z0-9 ]{3,20}$/;
	var ck_password =  /^[A-Za-z0-9!@#$%^&*()_]{5,20}$/;

	if(type == "login"){
		/* validate username */
		var username = document.getElementById("login-username").value;

		if (!ck_name.test(username)){
			console.info("username contains special chars");

			document.getElementById("login-username-lbl").innerHTML = "Username not valid";
			document.getElementById("login-username-lbl").classList.add("invalid-lable");

			document.getElementById("login-username").classList.add("invalid-input");

			errors[errors.length] = "username contains special chars";
		}
		else{
			console.info("username correct");

			document.getElementById("login-username-lbl").innerHTML = "Username";
			document.getElementById("login-username-lbl").classList.remove("invalid-lable");

			document.getElementById("login-username").classList.remove("invalid-input");
		}

		/* validate password */
		var password = document.getElementById("login-password").value;
		
		if (!ck_password.test(password)) {
			console.info("password not valid ");

			document.getElementById("login-password-lbl").innerHTML = "Password not valid";
			document.getElementById("login-password-lbl").classList.add("invalid-lable");

			document.getElementById("login-password").classList.add("invalid-input");

			errors[errors.length] = "password not valid";
		} else{
			console.info("password correct");

			document.getElementById("login-password-lbl").innerHTML = "Password";
			document.getElementById("login-password-lbl").classList.remove("invalid-lable");

			document.getElementById("login-password").classList.remove("invalid-input");			
		}
	} 

	else if(type == "register"){
		// var errors = [];

		/* validate username */
		var username = document.getElementById("reg-username").value;

		if (!ck_name.test(username)){
			console.info("username contains special chars");

			document.getElementById("reg-username-lbl").innerHTML = "Username not valid";
			document.getElementById("reg-username-lbl").classList.add("invalid-lable");

			document.getElementById("reg-username").classList.add("invalid-input");

			errors[errors.length] = "username contains special chars";
		}
		else{
			console.info("username correct");

			document.getElementById("reg-username-lbl").innerHTML = "Username";
			document.getElementById("reg-username-lbl").classList.remove("invalid-lable");

			document.getElementById("reg-username").classList.remove("invalid-input");
		}

		/* validate password */
		var password = document.getElementById("reg-password").value;
		var confirmPassword = document.getElementById("confirm_password").value;
		
		if (!ck_password.test(password)) {
			console.info("password not valid ");

			document.getElementById("reg-password-lbl").innerHTML = "Password not valid";
			document.getElementById("reg-password-lbl").classList.add("invalid-lable");

			document.getElementById("reg-password").classList.add("invalid-input");

			errors[errors.length] = "password not valid";
		} else{
			console.info("password correct");

			document.getElementById("reg-password-lbl").innerHTML = "Password";
			document.getElementById("reg-password-lbl").classList.remove("invalid-lable");	

			document.getElementById("reg-password").classList.remove("invalid-input");		
		}
		
		/* validate confirm password */
		 if (!ck_password.test(confirmPassword)) {
			console.info("passwords don't match");

			document.getElementById("reg-confirm-password-lbl").innerHTML = "Confirm password not valid";
			document.getElementById("reg-confirm-password-lbl").classList.add("invalid-lable");

			document.getElementById("confirm_password").classList.add("invalid-input");

			errors[errors.length] = "passwords don't match";
		} else if(password != confirmPassword){
			console.info("passwords don't match");

			document.getElementById("reg-confirm-password-lbl").innerHTML = "Passwords do not match";
			document.getElementById("reg-confirm-password-lbl").classList.add("invalid-lable");

			document.getElementById("confirm_password").classList.add("invalid-input");

			errors[errors.length] = "passwords don't match";
		} else{
			console.info("confirm password correct");

			document.getElementById("reg-confirm-password-lbl").innerHTML = "Confirm password";
			document.getElementById("reg-confirm-password-lbl").classList.remove("invalid-lable");

			document.getElementById("confirm_password").classList.remove("invalid-input");
		}
	}

	if (errors.length > 0) {
		return false;
	} else{
		return true;
	}  
}
